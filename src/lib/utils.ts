export const wait = async (timeInMs: number) => {
	return new Promise((r) => setTimeout(r, timeInMs));
};
