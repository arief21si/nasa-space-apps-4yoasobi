import * as motion from 'motion'

export const typeWriter = (node: HTMLElement, delay = 50) => {

  const textSplit = node.innerText.split('');
  node.innerHTML = '';

  textSplit?.forEach((char, index) => {
    setTimeout(() => {
      node.innerHTML += char;
    }, index * delay + (Math.random() * delay));
  });
}

export const _motion = (
  node: HTMLElement,
  props: {
    keyframes: motion.MotionKeyframesDefinition;
    options: motion.AnimationOptionsWithOverrides;
  }
) => {
  motion.animate(node, props.keyframes, props.options);
};
